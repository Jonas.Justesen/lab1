package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;


public class RockPaperScissors {
	
    public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 0;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public String userChoice() {
        while(true) {
            String userInput = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (validateChoice(userInput, rpsChoices)) {
                return(userInput);
            }
            else {
                System.out.println("I do not understand " + userInput + ". Could you try again?");
            }
        }
    }

    public String computerChoice() {
        /*
         * Generates random integer in [0, 2]
         * Integers correspond to list indexes in rpsChoices
        */ 
        return(rpsChoices.get(ThreadLocalRandom.current().nextInt(0,3)));
    }

    public boolean validateChoice(String humanChoice, List<String> rpsChoices) {
        return(rpsChoices.contains(humanChoice));
    }

    public boolean isWinner(String humanChoice, String computerChoice) {
        if (humanChoice.equals("rock") && (computerChoice.equals("scissors"))) {
            return(true);
        }
        if (humanChoice.equals("paper") && (computerChoice.equals("rock"))) {
            return(true);
        }
        if (humanChoice.equals("scissors") && (computerChoice.equals("paper"))) {
            return(true);
        }
        return(false);
    }

    public boolean continuePlaying(String continueAnswer) {
        while(true) {
            if (continueAnswer.equals("y")) {
                return(true);
            }
            else if (continueAnswer.equals("n")) {
                return(false);
            }
            else {
                System.out.println("I do not understand " + continueAnswer + ". Could you try again?");
            }
            
        }
    }

    public void run() {
        while (true) {
            roundCounter++;
            System.out.println("Let's play round " + roundCounter);
            String humanChoice = userChoice();
            String computerChoice = computerChoice();
            String choiceString = "Human chose " + humanChoice + ", computer chose " + computerChoice + ".";

            // Check winner
            if (isWinner(humanChoice, computerChoice)) {
                System.out.println(choiceString + " Human wins.");
                humanScore++;
            }
            else if (isWinner(computerChoice, humanChoice)) {
                System.out.println(choiceString + " Computer wins.");
                computerScore++;
            }
            else {
                System.out.println(choiceString + "It's a tie.");
            }

            // Print out score
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            // Ask user to continue playing or not
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (!continuePlaying(continueAnswer)) {
                System.out.println("Bye bye :)");
                break;
            }
        }
    }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}