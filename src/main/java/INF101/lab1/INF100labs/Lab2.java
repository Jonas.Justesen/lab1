package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        // findLongestWords("Hello", "basketball", "fotball");
        // System.out.println(isLeapYear(2022));
        findLongestWords("apple", "carrot", "ananas");

    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int lengthWord1 = word1.length();
        int lengthWord2 = word2.length();
        int lengthWord3 = word3.length();
        /*System.out.println(word1);
        System.out.println(lengthWord1);
        System.out.println(word2);
        System.out.println(lengthWord2);
        System.out.println(word3);
        System.out.println(lengthWord3);
        System.out.println("\n");*/


        if (lengthWord1 >= lengthWord2) {
            if (lengthWord1 >= lengthWord3) {
                System.out.println(word1);
            }
            if (lengthWord2 >= lengthWord3) {
                System.out.println(word2);
            }
            if (lengthWord3 >= lengthWord1) {
                System.out.println(word3);
            }
        }
        else {
            if (lengthWord2 >= lengthWord3) {
                System.out.println(word2);
            }
            if (lengthWord3 >= lengthWord3) {
                System.out.println(word3);
            }
        }
        
    }

    public static boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    return(true);
                }
                else {
                    return(false);
                }
            }
            else {
                return(true);
            }
        }
        else {
            return(false);
        }
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num > 0) {
            if (num % 2 == 0) {
                return(true);
            }
        }
        return(false);
    }

}
