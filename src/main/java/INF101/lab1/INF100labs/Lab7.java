package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        // Get common sum
        Integer commonSum = 0;
        for (int i = 0; i < (grid.get(0)).size(); i++) {
            commonSum += grid.get(0).get(i);
        }

        // Compare sums of each row to common sum
        Integer sumToBeCompared = 0;
        for (int i = 0; i < grid.size(); i++) {
            sumToBeCompared = 0;
            for (int j = 0; j < grid.get(i).size(); j++) {
                sumToBeCompared += grid.get(i).get(j);
            }
            if (sumToBeCompared != commonSum) {
                return(false);
            }
        }

        /* Compare sums of each column to common sum
         */
        for (int i = 0; i < grid.get(0).size(); i++) {
            sumToBeCompared = 0;
            for (int j = 0; j < grid.size(); j++) {
                sumToBeCompared += grid.get(j).get(i);
            }
            if (sumToBeCompared != commonSum) {
                return(false);
            }
        }
        return(true);
    }

}