package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        // multiplesOfSevenUpTo(50);
        // multiplicationTable(10);
        // System.out.println(crossSum(123));
    }

    public static void multiplesOfSevenUpTo(int n) {
        for (int i = 1; i < n+1; i += 1) {
            if (i % 7 == 0) {
                System.out.println(i);
            }
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i < n+1; i++) {
            System.out.print(i+": ");
            for (int x = 1; x < n+1; x++) {
                System.out.print((x*i)+" ");
            }
            System.out.println();
        }
    }

    public static int crossSum(int num) {
        String numString = String.valueOf(num);
        int sum = 0;
        for (int i = 0; i < numString.length(); i++) {
            sum += Character.getNumericValue(numString.charAt(i));
        }
        return (sum);
    }

}