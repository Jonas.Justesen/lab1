package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        // Create empty returnlist
        ArrayList<Integer> returnList = new ArrayList<>();
        /*  Iterate through each value in input list, and append
            each value multiplied with 2 to new empty returnlist*/
        for (int i : list) {
            returnList.add(i*2);
        }
        return(returnList);
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        // Create empty returnlist
        ArrayList<Integer> returnlist = new ArrayList<>();
        /*  Iterate through each value in input list, and append
            each value except 3 to new empty returnlist*/
        for (int i: list) {
            if (i != 3) {
                returnlist.add(i);
            }
        }
        return(returnlist);
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        // Create empty returnlist
        ArrayList<Integer> returnlist = new ArrayList<>();

        for (int i : list) {
            if (!returnlist.contains(i)) {
                returnlist.add(i);
            }
        }
        return(returnlist);
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        // Create empty returnlist
        Integer temporaryValueCache = 0;

        for (int i = 0; i < a.size(); i++) {
            temporaryValueCache = a.get(0) + b.get(i);
            a.remove(0);
            a.add(temporaryValueCache);
        }
    }

}